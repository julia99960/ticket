package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// CreateTicket 訂一張票
// @Summary Book One Ticket
// @Tags Ticket
// @version 1.0
// @produce application/json
// @param event_num formData int true "ticket number"
// @param user_id formData int true "user id"
// @param status formData int true "0/1"
// @Success 200 string string 成功的時候返回的值
// @Router /ticket [post]
func CreateTicket(c *gin.Context) {
	// 取得 ResponseRecorder 物件
	w := httptest.NewRecorder()
	eventnum, _ := strconv.Atoi(c.Request.FormValue("event_num"))
	userid, _ := strconv.Atoi(c.Request.FormValue("user_id"))
	status, _ := strconv.Atoi(c.Request.FormValue("status"))
	t := Ticket{
		UserID:   userid,
		EventNum: eventnum,
		Status:   status,
	}
	// Create 新增一筆訂票資訊
	boolRow, row := t.Create()
	if boolRow {
		c.JSON(http.StatusOK, gin.H{
			"code": w.Code,
			"msg":  fmt.Sprintf("insert successful %d", row),
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"code": w.Code,
			"msg":  "unsuccessful",
		})
	}
}

// GetTickets 取得使用者訂票紀錄
// @Summary Get User Ticket List
// @Tags Ticket
// @version 1.0
// @produce application/json
// @param user_id path int true "user id"
// @Success 200 string string 成功的時候返回的值
// @Router /ticket/{user_id}/tickets [get]
func GetTickets(c *gin.Context) {
	userid, _ := strconv.Atoi(c.Param("user_id"))
	tickets, err := GetTicketsList(userid)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{
			"ticket_list": tickets,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"ticket_list": nil,
		})
	}
}

// UpdateTicket 更改訂票紀錄 {0:註銷,1:正常}
// @Summary Update Ticket
// @Tags Ticket
// @version 1.0
// @produce application/json
// @param id path int true "ticker number"
// @param status formData int true "status Ex.0 or 1"
// @Success 200 string string 成功的時候返回的值
// @Router /ticket/{id}/status [post]
func UpdateTicket(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	status, _ := strconv.Atoi(c.Request.FormValue("status"))
	// 更改訂票紀錄
	row := UpdateTicketStatus(id, status)
	if row == 1 {
		c.JSON(http.StatusOK, gin.H{
			"msg": "success",
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg": "unsuccess",
		})
	}
}

// GetRemainTicket 總計某一場次剩餘票數
// @Summary Get Remain Ticket
// @Tags Remain Tickets
// @version 1.0
// @produce application/json
// @param event_num path int true "ticket detail number"
// @Success 200 string string 成功的時候返回的值候
// @Router /remain_tickets/{event_num} [get]
func GetRemainTicket(c *gin.Context) {
	eventnum, _ := strconv.Atoi(c.Param("event_num"))
	// 計算剩餘票數
	row := RemainTicket(eventnum)
	c.JSON(http.StatusOK, gin.H{
		"data": row,
	})
}

// AddOneDetail 新增一筆表演場次
// @Summary Add One Ticket Detail
// @Tags ticket detail
// @version 1.0
// @produce application/json
// @param title formData string true "ticket name"
// @param performer formData string true "author"
// @param ticket_price formData int true "ticket price"
// @param time_at formData string true "Use time EX.2020-12-01 13:00:00"
// @param book_from formData string true "Book from EX.2020-11-01 12:00:00"
// @param endbook_at formData string true "End Book Time Ex.2020-11-10 12:00:00"
// @param limit_seat formData int true "limit seat number Ex.250"
// @Success 200 string string 成功的時候返回的值
// @Router /ticket_detail [post]
func AddOneDetail(c *gin.Context) {
	title := c.Request.FormValue("title")
	performer := c.Request.FormValue("performer")
	price := c.Request.FormValue("ticket_price")
	timeat := c.Request.FormValue("time_at")
	bookfrom := c.Request.FormValue("book_from")
	endbookat := c.Request.FormValue("endbook_at")
	limitseat, _ := strconv.Atoi(c.Request.FormValue("limit_seat"))

	d := Detail{
		Title:     title,
		Performer: performer,
		Price:     price,
		TimeAt:    timeat,
		BookFrom:  bookfrom,
		EndbookAt: endbookat,
		LimitSeat: limitseat,
	}
	// 新增一筆表演資訊
	id := d.Create()
	msg := fmt.Sprintf("insert successful %d", id)
	c.JSON(http.StatusOK, gin.H{
		"msg": msg,
	})
}

// GetOneDetail 獲得詳細表演場次資訊
// @Summary Get One Detail
// @Tags ticket detail
// @version 1.0
// @produce application/json
// @param id path int true "ticket detail id"
// @Success 200 string string 成功的時候返回的值
// @Router /ticket_detail/{id} [get]
func GetOneDetail(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	// 獲取表演場次細節
	detail, err := GetPerformanceDetail(id)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{
			"result": detail,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"result": nil,
		})
	}
}

// AddOne 新增一位訂票者資訊
// @Summary Add One User
// @Tags User
// @version 1.0
// @produce application/json
// @param id_number formData string true "ID Card Number Ex.A123456789"
// @param mail formData string true "mail"
// @param name formData string true "name"
// @param birthday formData string true "birthday Ex.2000-01-24"
// @param status formData int true "status Ex.0 or 1"
// @Success 200 string string 成功的時候返回的值
// @Router /user [post]
func AddOne(c *gin.Context) {
	IDNumber := c.Request.FormValue("id_number")
	mail := c.Request.FormValue("mail")
	name := c.Request.FormValue("name")
	birthday := c.Request.FormValue("birthday")
	status, _ := strconv.Atoi(c.Request.FormValue("status"))
	u := User{
		IDNumber: IDNumber,
		Mail:     mail,
		Name:     name,
		Birth:    birthday,
		Status:   status,
	}
	// 新增一筆使用者資料
	row := u.AddOneUser()
	if row == 1 {
		c.JSON(http.StatusOK, gin.H{
			"msg": "success",
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg": "unsuccess",
		})

		startTime := time.Now().Format("2006-01-02 15:04:05")
		var d string
		if c.Request.Method == http.MethodPost { // 如果是post請求，則讀取body
			body, err := c.GetRawData() // body 只能讀一次，讀出來之後需要重置下 Body
			if err != nil {
				log.Fatal(err)
			}
			c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(body)) // 重置body
			d = string(body)
		}
		// 請求方式
		reqMethod := c.Request.Method
		// 請求路由
		reqURI := c.Request.RequestURI
		// 請求IP
		clientIP := c.ClientIP()
		errorlog := fmt.Sprintf("[%s] %s %s%s | Data: %s \n\n", startTime, reqMethod, clientIP, reqURI, d)
		WriteErrorLog(ErrLogPath, errorlog)
	}
}

// GetOne 取得一筆使用者資料
// @Summary Get One User
// @Tags User
// @version 1.0
// @produce application/json
// @param id path int true "user id"
// @Success 200 string string 成功的時候返回的值
// @Router /user/{id} [get]
func GetOne(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	// 取得一名使用者資料
	rs, err := GetOneUser(id)
	if err == nil {
		c.JSON(http.StatusOK, gin.H{
			"result": rs,
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"result": nil,
		})
		startTime := time.Now().Format("2006-01-02 15:04:05")
		d, err := c.GetRawData()

		if err != nil {
			fmt.Println(err.Error())
		}

		fmt.Printf("data: %v\n", string(d))

		c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(d))

		c.String(200, "ok")
		// 請求方式
		reqMethod := c.Request.Method
		// 請求路由
		reqURI := c.Request.RequestURI
		// 請求IP
		clientIP := c.ClientIP()
		errorlog := fmt.Sprintf("[%s] %s %s%s | Data: %s \n\n", startTime, reqMethod, clientIP, reqURI, string(d))
		WriteErrorLog(ErrLogPath, errorlog)
	}
}

// UpdateUser 更改訂票人狀態
// @Summary Update User status
// @Tags User
// @version 1.0
// @produce application/json
// @param id path int true "user id"
// @param status formData int true "status Ex.0 or 1"
// @Success 200 string string 成功的時候返回的值
// @Router /user/{id}/status [post]
func UpdateUser(c *gin.Context) {
	id, _ := strconv.Atoi(c.Param("id"))
	status, _ := strconv.Atoi(c.Request.FormValue("status"))
	// 更新使用者狀態
	row := UpdateUserStatus(id, status)
	if row == 1 {
		c.JSON(http.StatusOK, gin.H{
			"msg": "success",
		})
	} else {
		c.JSON(http.StatusOK, gin.H{
			"msg": "unsuccess",
		})
	}
}
