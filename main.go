package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	_ "ticket/docs"
	"time"

	"github.com/gin-contrib/cors"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	"github.com/gin-gonic/gin"
	_ "github.com/go-sql-driver/mysql"
)

var nFlag = flag.Int("n", 0, "help message for flag n")

//docker連線mysql
var (
	USERNAME = "root"
	PASSWORD = "demoroot"
	NETWORK  = "tcp"
	SERVER   = "127.0.0.1"
	PORT     = "3306"
	DATABASE = "ticket"
)

// fileName 文件檔案名稱
var fileName string = fmt.Sprintf("%s_errorlog", time.Now().Format("20060102"))

// ErrLogPath 文件檔案路徑
var ErrLogPath string = fmt.Sprintf("logs/%s.txt", fileName)

func init() {
	// 建立每日紀錄log的檔案
	if !FileExists(ErrLogPath) {
		file, err := os.Create(ErrLogPath)
		if err != nil {
			fmt.Println(err)
		}
		defer file.Close()
	}
}

// WriteErrorLog 紀錄 error log
func WriteErrorLog(fileName string, content string) error {
	f, err := os.OpenFile(fileName, os.O_WRONLY, 777)
	if err != nil {
		fmt.Println("file create failed. err: " + err.Error())
	} else {
		n, _ := f.Seek(0, os.SEEK_END)
		_, err = f.WriteAt([]byte(content), n)
	}
	defer f.Close()
	return err
}

// FileExists 判斷檔案是否存在
func FileExists(name string) bool {
	if _, err := os.Stat(name); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

/*
@title Ticket API
@version 1.0
@host 127.0.0.1:8000
@x-extension-openapi {"example": "value on a json format"}
*/
func main() {
	defer DB.Close()

	router := gin.Default()
	router.Use(cors.Default())

	//訂票紀錄
	router.POST("/ticket", CreateTicket)                      //一次下一張單
	router.GET("/ticket/:user_id/tickets", GetTickets)        //查詢訂票資料
	router.POST("/ticket/:id/status", UpdateTicket)           //更改訂單狀態
	router.GET("/remain_tickets/:event_num", GetRemainTicket) //總計某一場次剩餘票數

	//表演詳細資料
	router.POST("/ticket_detail", AddOneDetail)
	router.GET("/ticket_detail/:id", GetOneDetail)

	//訂票者資訊
	router.POST("/user", AddOne)
	router.GET("/user/:id", GetOne)
	router.POST("/user/:id/status", UpdateUser)

	//測試是否有成功啟動
	router.GET("/test", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "hellow world",
		})
	})

	//終止程式
	router.GET("/stop", func(c *gin.Context) {
		os.Exit(1)
	})

	// Listen and Server
	url := "127.0.0.1" // os.Getenv("APP_URL")
	port := "8000"     // os.Getenv("APP_PORT")
	serverURL := url + ":" + port

	// Debug Mode 才將 Swagger 路由導入
	// enable swagger API doc
	if mode := gin.Mode(); mode == gin.DebugMode {
		swagURL := ginSwagger.URL(fmt.Sprintf("http://%s/swagger/doc.json", serverURL))
		router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, swagURL))
	}
	go router.Run(serverURL)
	router.Run(":8000")
}
